import React, { useState } from 'react';
import FormInfo from '../models/FormInfo.interface';
import GeneralInfoInterface from '../models/GeneralInformation.interface';
import ConditionsInterface from '../models/Condition.interface';
import MedicalQuestionsInt from '../models/MedicalQuestion.interface';
import MedicalQuestionsInfo from '../mocks/medicalQuestions.json';

export const FormContext = React.createContext<FormInfo>({
    generalInformation: {
        firstName: "",
        lastName: "",
        gender: "",
        birthDate: "",
        email: "",
        phone: "",
        city: "",
        state: "",
        zipCode: "",
        maritalStatus: ""
    },
    conditions: [],
    medicalQuestions: [],
    isCurrentFormValid: false,
    acceptedTerms: false,
    updateGeneralInformation: (info: GeneralInfoInterface) => {},
    updateConditions: (conditions: ConditionsInterface[]) => {},
    updateMedicalQuestions: (medicalQuestions: MedicalQuestionsInt[]) => {},
    updateCurrentFormValid: (value: boolean) => {},
    updateTerms: (value: boolean) => {},
});

const FormContextProvider: React.FC = (props) => {
    const generalInfoObj = {
        firstName: "",
        lastName: "",
        gender: "",
        birthDate: "",
        email: "",
        phone: "",
        city: "",
        state: "",
        zipCode: "",
        maritalStatus: ""
    }
    const [generalInformation, setGeneralInformation] = useState<GeneralInfoInterface>(generalInfoObj);
    const [conditions, setConditions ] = useState<ConditionsInterface[]>([]);
    const [medicalQuestions, setMedicalQuestions] = useState<MedicalQuestionsInt[]>(MedicalQuestionsInfo);
    const [isCurrentFormValid, setIsCurrentFormValid] = useState<boolean>(false);
    const [acceptedTerms, setAcceptedTerms] = useState<boolean>(false);

    // const updateGeneralInfoHandler = (generalInfo: GeneralInfoInterface) => {
    //     setGeneralInformation(generalInfo);
    // }

    const updateConditionsHandler = (conditions: ConditionsInterface[]) => {
        setConditions(conditions);
    }
    const updateMedicalQuestionsHandler = (medicalQuestions: MedicalQuestionsInt[]) => {
        setMedicalQuestions(medicalQuestions);
    }

    const updateTermsHandler = (isAccepted: boolean) => {
        setAcceptedTerms(isAccepted);
    }

    const contextValue: FormInfo = {
        generalInformation: generalInformation,
        conditions: conditions,
        medicalQuestions: medicalQuestions,
        isCurrentFormValid: isCurrentFormValid,
        acceptedTerms: acceptedTerms,
        updateGeneralInformation: (generalInfo: GeneralInfoInterface) => setGeneralInformation(generalInfo),
        updateConditions: updateConditionsHandler,
        updateMedicalQuestions: updateMedicalQuestionsHandler,
        updateCurrentFormValid: setIsCurrentFormValid,
        updateTerms: updateTermsHandler
    }

    return(
        <FormContext.Provider value={contextValue}>
            {props.children}
        </FormContext.Provider>
    )
}

export default FormContextProvider;
