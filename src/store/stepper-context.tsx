import React, { useState } from 'react';
import Step from '../models/Step.interface';

export const StepContext = React.createContext<Step>({
    currentStep: 0,
    nextStep: () => {},
    prevStep: () => {}
});

const StepContextProvider: React.FC = (props) => {
    const [currentStep, setCurrentStep ] = useState<number>(0);

    const nextStepHandler = () => {
        if((currentStep + 1) !== 5) {
            setCurrentStep(currentStep + 1);
        }
    }

    const prevStepHandler = () => {
        if((currentStep - 1) !== -1) {
            setCurrentStep(currentStep - 1);
        }
    }

    const contextValue = {
        currentStep: currentStep,
        nextStep: nextStepHandler,
        prevStep: prevStepHandler
    }

    return(
        <StepContext.Provider value={contextValue}>
            {props.children}
        </StepContext.Provider>
    )
}

export default StepContextProvider;



