import React from 'react';
import './App.css';
import MainContainer from './components/MainContainer';
import { StylesProvider } from '@mui/styles';

function App() {
  return (
    <div className="App"> 
      <StylesProvider injectFirst >
          <MainContainer/>
      </StylesProvider>
    </div>
  );
}

export default App;
