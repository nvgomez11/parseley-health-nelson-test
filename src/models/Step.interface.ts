interface Step {
    currentStep: number,
    nextStep: () => void;
    prevStep: () => void;
}

export default Step;