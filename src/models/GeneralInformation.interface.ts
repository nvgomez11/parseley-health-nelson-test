interface GeneralInformation {
    firstName: string,
    lastName: string,
    gender: string,
    birthDate: string,
    email: string,
    phone: string,
    city: string,
    state: string,
    zipCode: string,
    maritalStatus: string
}

export default GeneralInformation;