export default interface MedicalQuestion {
    question: string;
    subquestion: string;
    suffer?: boolean;
    answer?: string;
}