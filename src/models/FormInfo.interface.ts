import GeneralInformation from './GeneralInformation.interface';
import ConditionsInterface from './Condition.interface';
import MedicalQuestions from './MedicalQuestion.interface';

interface FormInfo {
    generalInformation: GeneralInformation,
    conditions: ConditionsInterface[],
    medicalQuestions: MedicalQuestions[],
    isCurrentFormValid: boolean,
    acceptedTerms: boolean
    updateGeneralInformation: (info: GeneralInformation) => void,
    updateConditions: (conditions: ConditionsInterface[]) => void,
    updateMedicalQuestions: (medicalQuestions: MedicalQuestions[]) => void,
    updateCurrentFormValid: (value: boolean) => void,
    updateTerms: (value: boolean) => void    
}

export default FormInfo;