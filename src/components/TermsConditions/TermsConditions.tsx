import { useContext } from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import Checkbox from '@mui/material/Checkbox';
import { FormContext } from '../../store/form-context';


const termsConditions = `Nullam quis risus eget urna mollis ornare vel eu leo. Aenean lacinia bibendum nulla sed consectetur. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Etiam porta sem malesuada magna mollis euismod. Maecenas sed diam eget risus varius blandit sit amet non magna.
                        Etiam porta sem malesuada magna mollis euismod. Nullam quis risus eget urna mollis ornare vel eu leo. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Vestibulum id ligula porta felis euismod semper. Donec ullamcorper nulla non metus auctor fringilla.`

const TermsConditions = () => {
    const formInfoCtx = useContext(FormContext);

    const termsConditionsHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        formInfoCtx.updateTerms(event.target.checked);
    }

    return(
        <Card sx={{ minWidth: 275 }}>
            <CardContent>
                <Typography align='left' sx={{ mb: 1.5 }} variant="h5" component="div">
                    Terms and Conditions
                </Typography>
                <Typography align='left' variant="body2">
                    {termsConditions}
                </Typography>
            </CardContent>
            <CardActions>
                <Checkbox
                    checked={formInfoCtx.acceptedTerms ? true : false} 
                    onChange={ (event) => termsConditionsHandler(event)}
                />
                <Typography align='left' sx={{ fontSize: 17, fontWeight: 700 }} variant="h5" component="div">
                    Agree to the terms and conditions
                </Typography>
            </CardActions>
      </Card>
    )
}

export default TermsConditions;