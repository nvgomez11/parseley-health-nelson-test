import React, { useState, useEffect, useContext } from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import Checkbox from '@mui/material/Checkbox';
import ListItemText from '@mui/material/ListItemText';
import TextField from '@mui/material/TextField';
import style from './MedicalQuestions.module.css';
import QuestionsInterface from '../../models/MedicalQuestion.interface';
import { FormContext } from '../../store/form-context';


const MedicalQuestions = () => {
    const formInfoCtx = useContext(FormContext);
    const [medicalQuestions, setMedicalQuestion ] = useState<QuestionsInterface[]>(formInfoCtx.medicalQuestions);

    useEffect(() => {
        formInfoCtx.updateMedicalQuestions(medicalQuestions);
    }, [medicalQuestions]);

    const questionChangeHandler = (event: React.ChangeEvent<HTMLInputElement>, question: QuestionsInterface) => {
        const currentQuestions = [...medicalQuestions];
        const indexElement = currentQuestions.indexOf(question);
        if(indexElement !== -1) {
            currentQuestions[indexElement].suffer = !question.suffer;
            setMedicalQuestion(currentQuestions);
        }
    }

    const textChangeHandler = (event: React.FocusEvent<HTMLInputElement | HTMLTextAreaElement>, question: QuestionsInterface) => {
        const currentQuestions= [...medicalQuestions];
        const indexElement = currentQuestions.indexOf(question);
        if(indexElement !== -1) {
            currentQuestions[indexElement].answer = event.target.value;
            setMedicalQuestion(currentQuestions);
        }
    }

    return(    
        <List>
            {medicalQuestions.map( (question) => {
                return (
                    <ListItem className={style.flexColumn} key={question.question} >
                        <div className={style.flexRow}>
                            <ListItemText primary={question.question}/>
                            <Checkbox
                                onChange={ (event) => questionChangeHandler(event, question)}
                                checked={question.suffer ? true : false}
                            />
                        </div>
                        { question.suffer && 
                            <TextField
                                className = {style.fullWidth}
                                id="outlined-textarea"
                                label={question.subquestion}
                                placeholder={question.subquestion}
                                multiline
                                onBlur = { (event) => textChangeHandler(event, question)}
                                value={question.answer}
                            />
                        }
                    </ListItem>
                )
            })}
        </List>
    )
}

export default MedicalQuestions;
