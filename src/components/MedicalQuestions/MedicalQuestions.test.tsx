import Enzyme from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import { mount } from 'enzyme';
import MedicalQuestions from './MedicalQuestions';
import FormContextProvider from './../../store/form-context';
import questionsData from '../../mocks/medicalQuestions.json';
import ListItem from '@mui/material/ListItem';
import Checkbox from '@mui/material/Checkbox';
import TextField from '@mui/material/TextField';

Enzyme.configure({ adapter: new Adapter() });
describe('(Component) Medical Questions', () => {
    test('render questions correctly', () => {        
        const wrapper = mount(
            <FormContextProvider>
                <MedicalQuestions/>
            </FormContextProvider>
        );

        const expectedQuestionsLength = questionsData.length;
        const questions = wrapper.find(ListItem);
        expect(questions.length).toBe(expectedQuestionsLength);
    });

    test('should render without textfields by default', () => {
        const wrapper = mount(
            <FormContextProvider>
                <MedicalQuestions/>
            </FormContextProvider>
        );

        const textFields = wrapper.find(TextField);
        expect(wrapper.find(TextField).length).toBe(0);
    })


    test('renders checkboxes correctly', () => {
        const wrapper = mount(
            <FormContextProvider>
                <MedicalQuestions/>
            </FormContextProvider>
        );

        const expectedQuestionsLength = questionsData.length;
        const checkboxes = wrapper.find(Checkbox);
        expect(wrapper.find(Checkbox).length).toBe(expectedQuestionsLength);
    });
})