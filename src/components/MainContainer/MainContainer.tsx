import MultiStepForm from "../MultiStepForm/MultiStepForm";
import styles from './MainContainer.module.css';
import StepContextProvider from '../../store/stepper-context';
import FormContextProvider from "../../store/form-context";

const MainContainer = () => {

    return(
        <StepContextProvider>
            <FormContextProvider>
                <div className={ styles.mainContainer}>
                    <MultiStepForm/>
                </div>
            </FormContextProvider>
        </StepContextProvider>
    )
}

export default MainContainer;