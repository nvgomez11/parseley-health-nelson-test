import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import Button from '@mui/material/Button';
import style from './Conditions.module.css';
import React, { useState, useEffect, useContext } from 'react';
import ConditionsInterface from '../../models/Condition.interface'
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import DeleteIcon from '@mui/icons-material/Delete';
import IconButton from '@mui/material/IconButton';
import { FormContext } from '../../store/form-context';
import ConditionsList from '../../mocks/conditions.json';


const Conditions = () => {
    const formInfoCtx = useContext(FormContext);

    const [conditionsList, setConditionsList] = useState<ConditionsInterface[]>(ConditionsList);
    const [selectedCondition, setSelectedCondition] = useState<ConditionsInterface | null>(null);
    const [addedConditions, setAddedConditions] = useState<ConditionsInterface[]>(formInfoCtx.conditions);

    useEffect(() => {
        if(addedConditions.length !== 0) {
            formInfoCtx.updateConditions(addedConditions);
        }
    }, [addedConditions]);

    const selectedConditionHandler = (event: React.SyntheticEvent, condition: ConditionsInterface | null) => {
        setSelectedCondition(condition);
    }

    const addConditionHandler = () => {
        if(selectedCondition) {
            if(!addedConditions.includes(selectedCondition)){
                const newConditions = [...addedConditions, selectedCondition];
                setAddedConditions(newConditions);
                setSelectedCondition(null);
            }
        }
    }

    const onDeleteConditionHandler = (id: string) => {
        const conditions = [...addedConditions];
        const filteredConditions = conditions.filter((condition) => {
            return condition.condition !== id;
        });
        setAddedConditions(filteredConditions);
    }

    const showAddedConditionsList = () => {
        return(
            <List className={style.container}>
                {addedConditions.map( (condition) => {
                    return (
                        <ListItem key={condition.condition}>
                            <ListItemText primary={condition.condition} secondary={condition.type} className={style.centered}/>
                            <IconButton edge="end" aria-label="delete" onClick={() => onDeleteConditionHandler(condition.condition)}>
                                <DeleteIcon />
                            </IconButton>
                        </ListItem>
                    )
                })}
            </List>
        )
    }

    return(
        <form className={style.form}>
            <div className={style.condition}>
                <Autocomplete
                    id="grouped-conditions"
                    onChange = { (event, values) => selectedConditionHandler(event, values)}
                    options={conditionsList.sort((a, b) => -b.type.localeCompare(a.type))}
                    groupBy={(condition) => condition.type}
                    getOptionLabel={(condition) => condition.condition}
                    sx={{ width: 300 }}
                    renderInput={(params) => <TextField {...params} label="Conditions" />}
                />
                <Button
                    onClick={addConditionHandler}
                    variant="contained"
                >Add Condition</Button>
            </div>
            <div>
                { addedConditions.length > 0 ? showAddedConditionsList() : <p>Please select the conditions</p>}
            </div>
        </form>
    )
}

export default Conditions;
