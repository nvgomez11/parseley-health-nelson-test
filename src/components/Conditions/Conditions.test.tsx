import Enzyme from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import Conditions from './Conditions';
import { mount } from 'enzyme';
import FormContextProvider from './../../store/form-context';
import List from '@mui/material/List';
import { FormContext } from '../../store/form-context';
import GeneralInfoInterface from '../../models/GeneralInformation.interface';
import ConditionsInterface from '../../models/Condition.interface';
import MedicalQuestionsInt from '../../models/MedicalQuestion.interface';
import { useContext } from 'react';

const initialState = {
    generalInformation: {
        firstName: "",
        lastName: "",
        gender: "",
        birthDate: "",
        email: "",
        phone: "",
        city: "",
        state: "",
        zipCode: "",
        maritalStatus: ""
    },
    conditions: [],
    medicalQuestions: [],
    isCurrentFormValid: false,
    acceptedTerms: false,
    updateGeneralInformation: (info: GeneralInfoInterface) => {},
    updateConditions: (conditions: ConditionsInterface[]) => {},
    updateMedicalQuestions: (medicalQuestions: MedicalQuestionsInt[]) => {},
    updateCurrentFormValid: (value: boolean) => {},
    updateTerms: (value: boolean) => {},
};

Enzyme.configure({ adapter: new Adapter() });

describe('(Component) Conditions', () => {
    test('conditions list empty by default', () => {
        const wrapper = mount(
            <FormContextProvider>
                <Conditions />
            </FormContextProvider>
        );

        const conditionsList = wrapper.find(List);
        expect(conditionsList.exists()).toBe(false);
    });

    test('shows selected conditions', () => {        
        const state = {
            ...initialState,
            conditions: [
                {type: 'cardiovascular', condition: 'Coronary heart disease'}
            ]
        }
        const wrapper = mount(
            <FormContext.Provider value={state}>
                <Conditions />
            </FormContext.Provider>
        );

        const conditionsList = wrapper.find(List);
        expect(conditionsList.exists()).toBe(true);
    });
})