import Enzyme from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import { mount } from 'enzyme';
import { StepContext } from '../../store/stepper-context';
import MultiStepForm from './MultiStepForm';
import PersonalInformation from '../PersonalInformation';
import Conditions from '../Conditions';
import MedicalQuestions from '../MedicalQuestions';
import Summary from '../Summary';
import Terms from '../TermsConditions';

Enzyme.configure({ adapter: new Adapter() });
describe('(Component) MultiStepForm', () => { 
    test('renders Personal Info correctly', () => {
        const stepMockValue = {
            currentStep: 0,
            nextStep: () => {},
            prevStep: () => {}
        }        
        const wrapper = mount(
            <StepContext.Provider value={stepMockValue}>
                <MultiStepForm/>
            </StepContext.Provider>
        );

        const component = wrapper.find(PersonalInformation);
        expect(component.exists()).toBe(true);
    });

    test('renders Conditions correctly', () => {
        const stepMockValue = {
            currentStep: 1,
            nextStep: () => {},
            prevStep: () => {}
        }        
        const wrapper = mount(
            <StepContext.Provider value={stepMockValue}>
                <MultiStepForm/>
            </StepContext.Provider>
        );

        const component = wrapper.find(Conditions);
        expect(component.exists()).toBe(true);
    });

    test('renders Medical Questions correctly', () => {
        const stepMockValue = {
            currentStep: 2,
            nextStep: () => {},
            prevStep: () => {}
        }        
        const wrapper = mount(
            <StepContext.Provider value={stepMockValue}>
                <MultiStepForm/>
            </StepContext.Provider>
        );

        const component = wrapper.find(MedicalQuestions);
        expect(component.exists()).toBe(true);
    });

    test('renders Summary correctly', () => {
        const stepMockValue = {
            currentStep: 3,
            nextStep: () => {},
            prevStep: () => {}
        }        
        const wrapper = mount(
            <StepContext.Provider value={stepMockValue}>
                <MultiStepForm/>
            </StepContext.Provider>
        );

        const component = wrapper.find(Summary);
        expect(component.exists()).toBe(true);
    });

    test('renders Terms correctly', () => {
        const stepMockValue = {
            currentStep: 4,
            nextStep: () => {},
            prevStep: () => {}
        }        
        const wrapper = mount(
            <StepContext.Provider value={stepMockValue}>
                <MultiStepForm/>
            </StepContext.Provider>
        );

        const component = wrapper.find(Terms);
        expect(component.exists()).toBe(true);
    });
})