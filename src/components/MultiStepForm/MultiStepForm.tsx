import { useContext } from 'react';
import Box from '@mui/material/Box';
import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import StepLabel from '@mui/material/StepLabel';
import FormControls from '../FormControls';
import PersonalInformation from '../PersonalInformation';
import Conditions from '../Conditions/Conditions';
import MedicalQuestions from '../MedicalQuestions';
import Summary from '../Summary';
import TermsConditions from '../TermsConditions';
import { StepContext } from './../../store/stepper-context';
import styles from './MultiStepForm.module.css';


const steps = [
    'General Information',
    'Conditions',
    'Medical Questions',
    'Summary',
    'Terms & Conditions',
];

const MultiStepForm = () => {
    const stepCtx = useContext(StepContext);
    const currentStep = stepCtx.currentStep

    const getCurrentStep = () => {
        switch(currentStep) {
            case 0:
                return <PersonalInformation/>

            case 1:
                return <Conditions/>

            case 2:
                return <MedicalQuestions/>

            case 3:
                return <Summary/>

            case 4:
                return <TermsConditions/>

            default:
                return <PersonalInformation/>
        }
    };

    return(
        <div className={styles.form}>
            <Box sx={{ width: '100%' }}>
                <Stepper activeStep={currentStep} alternativeLabel className={styles.stepperMargin}>
                    {steps.map((label) => (
                    <Step key={label}>
                        <StepLabel>{label}</StepLabel>
                    </Step>
                    ))}
                </Stepper>
            </Box>
            { getCurrentStep() }
            <FormControls/>
        </div>
    )
}

export default MultiStepForm;