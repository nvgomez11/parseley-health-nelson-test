import TextField from '@mui/material/TextField';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import styles from './PersonalInformation.module.css';
import { useState, useContext, useEffect } from 'react';
import { FormContext } from '../../store/form-context';

// const genders = [
//     { label: 'Male'},
//     { label: 'Female'},
//     { label: 'Other'}
// ]

// const maritalStatus = [
//     { label: 'Married'},
//     { label: 'Single'},
//     { label: 'Divorced'},
//     { label: 'Life Partner'},
//     { label: 'Separated'},
//     { label: 'Widowed'},
//     { label: 'Other'},
// ]

const PersonalInformation = () => {
    const formInfoCtx = useContext(FormContext);
    const generalInfo = formInfoCtx.generalInformation;

    const [name, setName] = useState(generalInfo.firstName);
    const [lastName, setLastName] = useState(generalInfo.lastName);
    const [gender, setGender] = useState(generalInfo.gender);
    const [dateBirth, setDateBirth] = useState(generalInfo.birthDate);
    const [email, setEmail] = useState(generalInfo.email);
    const [phone, setPhone] = useState(generalInfo.phone);
    const [city, setCity] = useState(generalInfo.city);
    const [state, setState] = useState(generalInfo.state);
    const [zipCode, setZipCode] = useState(generalInfo.zipCode);
    const [maritalStatus, setMaritalStatus] = useState(generalInfo.maritalStatus);

    useEffect(() => {
        const validationTimer = setTimeout( () => {
            if(validateForm()){
                const generalInfo = {
                    firstName: name,
                    lastName: lastName,
                    gender,
                    birthDate: dateBirth,
                    email,
                    phone,
                    city,
                    state,
                    zipCode,
                    maritalStatus
                }
                formInfoCtx.updateGeneralInformation(generalInfo);
                formInfoCtx.updateCurrentFormValid(true);
            }
        }, 1000);

        return () => {
            clearTimeout(validationTimer);
        }
        
    }, [name, lastName, gender, dateBirth, email, phone, city, state, zipCode, maritalStatus])

    const validateEmail = () => {
        const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex.test(String(email).toLowerCase());
    };

    const validateForm = () => {
        if (
          name !== "" &&
          lastName !== "" &&
          gender !== "" &&
          dateBirth !== "" &&
          validateEmail() &&
          phone !== "" &&
          city !== "" &&
          state !== "" &&
          zipCode !== "" &&
          maritalStatus !== ""
        ) {
            return true;
        }
        return false;
    };

    const onChangeNameHandler = (event : React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
        setName(event.target.value);
    }

    const onLastNameHandler = (event : React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
        setLastName(event.target.value);
    }

    const onDateHandler = (event : React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
        setDateBirth(event.target.value)

    }
    const onEmailHandler = (event : React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
        setEmail(event.target.value)

    }
    const onPhoneHandler = (event : React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
        setPhone(event.target.value)

    }
    const onCityHandler = (event : React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
        setCity(event.target.value)

    }
    const onStateHandler = (event : React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
        setState(event.target.value)

    }
    const onZipCodeHandler = (event : React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
        setZipCode(event.target.value)

    }
    
    return(
        <form className={styles.form}>
            <div className={styles.flexRow}>
                <TextField 
                    type="text"
                    className={styles.generalInput} 
                    onChange={(event) => {onChangeNameHandler(event)}} 
                    id="name" 
                    label="Name" 
                    value={name} 
                    variant="outlined" />
                <TextField 
                    type="text"
                    className={styles.generalInput}
                    onChange={(event) => {onLastNameHandler(event)}} 
                    id="lastName" 
                    label="Last Name" 
                    value={lastName} 
                    variant="outlined" />
            </div>
            <div className={styles.flexRow}>
                <FormControl className={styles.generalInput}>
                    <InputLabel id="gender-label">Gender</InputLabel>
                    <Select
                        labelId="gender-label"
                        id="gender"
                        label="gender"
                        value={gender}
                        onChange={(event) => {setGender(event.target.value as string)}}
                    >
                        <MenuItem value={"Male"}>Male</MenuItem>
                        <MenuItem value={"Female"}>Female</MenuItem>
                        <MenuItem value={"Other"}>Other</MenuItem>
                    </Select>
                </FormControl>
                <TextField
                    className={styles.generalInput}
                    type="date"
                    id="dateOfBirth"
                    label="Date of Birth"
                    variant="outlined"
                    value={dateBirth}
                    onChange={(event) => {onDateHandler(event)}}
                    InputLabelProps={{
                        shrink: true,
                    }}
                />
            </div>
            <div className={styles.flexRow}>
                <TextField 
                    className={styles.generalInput} 
                    onChange={(event) => {onEmailHandler(event)}}
                    value={email}
                    id="email" 
                    label="Email" 
                    variant="outlined" />
                <TextField 
                    className={styles.generalInput}
                    onChange={(event) => {onPhoneHandler(event)}}
                    value={phone}
                    id="phone" 
                    label="Phone" 
                    variant="outlined" />
            </div>
            <div className={styles.flexRow}>
                <TextField 
                    className={styles.generalInput}
                    onChange={(event) => {onCityHandler(event)}}
                    value={city}
                    id="city" 
                    label="City" 
                    variant="outlined" />
                <TextField 
                    className={styles.generalInput}
                    onChange={(event) => {onStateHandler(event)}}
                    value={state}
                    id="state" 
                    label="State" 
                    variant="outlined" />
            </div>
            <div className={styles.flexRow}>
                <TextField 
                    className={styles.generalInput}
                    onChange={(event) => {onZipCodeHandler(event)}}
                    value={zipCode}
                    id="zipcode" 
                    label="Zip code" 
                    variant="outlined" />
                <FormControl className={styles.generalInput}>
                    <InputLabel id="status-label">Marital Status</InputLabel>
                    <Select
                        labelId="status-label"
                        id="maritalStatus"
                        label="maritalStatus"
                        onChange={(event) => setMaritalStatus(event.target.value as string)}
                        value={maritalStatus}
                    >
                        <MenuItem value={"Married"}>Married</MenuItem>
                        <MenuItem value={"Single"}>Single</MenuItem>
                        <MenuItem value={"Divorced"}>Divorced</MenuItem>
                        <MenuItem value={"Life Partner"}>Life Partner</MenuItem>
                        <MenuItem value={"Separated"}>Separated</MenuItem>
                        <MenuItem value={"Widowed"}>Widowed</MenuItem>
                        <MenuItem value={"Other"}>Other</MenuItem>
                    </Select>
                </FormControl>
            </div>
        </form>
    )
}



export default PersonalInformation;