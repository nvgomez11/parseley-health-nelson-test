import Enzyme from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import PersonalInformation from './PersonalInformation';
import Select from '@mui/material/Select';
import { mount } from 'enzyme';
import FormContextProvider from './../../store/form-context';
import TextField from '@mui/material/TextField';

Enzyme.configure({ adapter: new Adapter() });

describe('(Component) Personal Information', () => {
    test('renders correctly', () => {
        const wrapper = mount(
            <FormContextProvider>
                <PersonalInformation />
            </FormContextProvider>
        );
        const componentInputs = wrapper.find(TextField);
        expect(componentInputs.length).toBe(8);

        const componentSelects = wrapper.find(Select);
        expect(componentSelects.length).toBe(2);
    });
})