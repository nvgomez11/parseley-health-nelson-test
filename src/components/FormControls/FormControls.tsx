import React,{ useContext } from 'react';
import { StepContext } from './../../store/stepper-context';
import { FormContext } from '../../store/form-context';
import Button from '@mui/material/Button';
import styles from './FormControls.module.css';


const FormControls: React.FC = () => {
    const stepCtx = useContext(StepContext);
    const formCtx = useContext(FormContext);

    const finishFormHandler = () => {
        const formResult = {
            generalInformation: formCtx.generalInformation,
            conditions: formCtx.conditions,
            medicalQuestions: formCtx.medicalQuestions,
            acceptedTerms: formCtx.acceptedTerms
        }
        console.log("Form completed, sending to API...", formResult);
    }

    const handleNextButtonClick = () => {
        if(stepCtx.currentStep !== 4) {
            stepCtx.nextStep();
        }else{
            finishFormHandler();
        }
    }

    const handleDisabledButton = () => {
        if(stepCtx.currentStep === 0) {
            if(formCtx.isCurrentFormValid) {
                return false;                
            } else {
                return true;
            }
        }

        if(stepCtx.currentStep === 4 ) {
            if(formCtx.acceptedTerms) {
                return false;
            } else {
                return true;
            }
        }
    }

    return(
        <div className={ styles.controlsContainer}>
            <Button disabled={stepCtx.currentStep === 0 ? true : false} variant="contained" onClick = { () => stepCtx.prevStep()}>Back</Button>
            <Button disabled={handleDisabledButton()} variant="contained" onClick = { () => handleNextButtonClick()}>{stepCtx.currentStep !== 4 ? "Next" : "Finish"}</Button>         
        </div>
    )
}

export default FormControls;