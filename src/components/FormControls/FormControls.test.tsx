import Enzyme from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import { mount } from 'enzyme';
import FormControls from './FormControls';
import FormContextProvider from './../../store/form-context';
import StepContextProvider from '../../store/stepper-context';
import Button from '@mui/material/Button';
import { StepContext } from '../../store/stepper-context';

Enzyme.configure({ adapter: new Adapter() });
describe('(Component) FormControls', () => {    
    test('renders component correctly', () => {        
        const wrapper = mount(
            <StepContextProvider>
                <FormContextProvider>
                    <FormControls/>
                </FormContextProvider>
            </StepContextProvider>
        );

        const buttons = wrapper.find(Button);
        expect(buttons.length).toBe(2);
    });

    test('back button disabled on step 0', () => {
        const wrapper = mount(
            <StepContextProvider>
                <FormContextProvider>
                    <FormControls/>
                </FormContextProvider>
            </StepContextProvider>
        );

        const backButton = wrapper.find(Button).first();
        expect(backButton.props().disabled).toBe(true);
    })

    test('changes next button label in last step', () => {
        const mockStepValue = {
            currentStep: 4,
            nextStep: () => {},
            prevStep: () => {}
        }
    
        const wrapper = mount(
            <StepContext.Provider value={mockStepValue}>            
                    <FormControls/>
            </StepContext.Provider>
        );

        expect(wrapper.find(Button).last().text()).toEqual("Finish");
    })
})