import React, { useContext } from 'react';
import { FormContext } from '../../store/form-context';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import styles from './Summary.module.css';

const Summary = () => {
    const formInfoCtx = useContext(FormContext);
    return(
        <div>
            <Accordion expanded>
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                    >
                    <Typography sx={{fontSize: 20, fontWeight: 700}}>General Information</Typography>
                </AccordionSummary>
                <AccordionDetails>
                <Box sx={{ flexGrow: 1 }}>
                    <Grid container spacing={2}>
                        <Grid item xs={6}>
                            <Typography align='left' sx={{ fontSize: 17, mr: 2, fontWeight: 600}} color="text.primary">
                                Name
                            </Typography>
                            <Typography align='left'>
                                {formInfoCtx.generalInformation.firstName}
                            </Typography>
                        </Grid>
                        <Grid item xs={6}>
                            <Typography align='left' sx={{ fontSize: 17, mr: 2, fontWeight: 600}} color="text.primary">
                                Last name  
                            </Typography>
                            <Typography align='left'>
                                {formInfoCtx.generalInformation.firstName}
                            </Typography>
                        </Grid>
                    </Grid>
                </Box>
                <Box>
                    <Grid container spacing={2}>
                        <Grid item xs={6}>
                            <Typography align='left' sx={{ fontSize: 17, mr: 2, fontWeight: 600}} color="text.primary">
                                Gender  
                            </Typography>
                            <Typography align='left'>
                                {formInfoCtx.generalInformation.gender}
                            </Typography>
                        </Grid>
                        <Grid item xs={6}>
                            <Typography align='left' sx={{ fontSize: 17, mr: 2, fontWeight: 600}} color="text.primary">
                                Date of Birth  
                            </Typography>
                            <Typography align='left'>
                                {formInfoCtx.generalInformation.birthDate}
                            </Typography>
                        </Grid>
                    </Grid>
                </Box>
                <Box>
                    <Grid container spacing={2}>
                        <Grid item xs={6}>
                            <Typography align='left' sx={{ fontSize: 17, mr: 2, fontWeight: 600}} color="text.primary">
                                Email  
                            </Typography>
                            <Typography align='left'>
                                {formInfoCtx.generalInformation.email}
                            </Typography>
                        </Grid>
                        <Grid item xs={6}>
                            <Typography align='left' sx={{ fontSize: 17, mr: 2, fontWeight: 600}} color="text.primary">
                                Phone  
                            </Typography>
                            <Typography align='left'>
                                {formInfoCtx.generalInformation.phone}
                            </Typography>
                        </Grid>
                    </Grid>
                </Box>
                <Box>
                    <Grid container spacing={2}>
                        <Grid item xs={6}>
                            <Typography align='left' sx={{ fontSize: 17, mr: 2, fontWeight: 600}} color="text.primary">
                                City
                            </Typography>
                            <Typography align='left'>
                                {formInfoCtx.generalInformation.city}
                            </Typography>
                        </Grid>
                        <Grid item xs={6}>
                            <Typography align='left' sx={{ fontSize: 17, mr: 2, fontWeight: 600}} color="text.primary">
                                State  
                            </Typography>
                            <Typography align='left'>
                                {formInfoCtx.generalInformation.state}
                            </Typography>
                        </Grid>
                    </Grid>
                </Box>
                <Box>
                    <Grid container spacing={2}>
                        <Grid item xs={6}>
                            <Typography align='left' sx={{ fontSize: 17, mr: 2, fontWeight: 600}} color="text.primary">
                                Zip Code  
                            </Typography>
                            <Typography align='left'>
                                {formInfoCtx.generalInformation.zipCode}
                            </Typography>
                        </Grid>
                        <Grid item xs={6}>
                            <Typography align='left' sx={{ fontSize: 17, mr: 2, fontWeight: 600}} color="text.primary">
                                Marital Status
                            </Typography>
                            <Typography align='left'>
                                {formInfoCtx.generalInformation.maritalStatus}
                        </Typography>
                        </Grid>
                    </Grid>
                </Box>
                </AccordionDetails>
            </Accordion>

            <Accordion expanded>
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                    >
                    <Typography sx={{fontSize: 20, fontWeight: 700}}>Conditions</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <List>
                        {   formInfoCtx.conditions.length !== 0 ? 
                            formInfoCtx.conditions.map( (condition) => {
                                return (
                                    <ListItem key={condition.condition}>
                                        <ListItemText primary={condition.condition} secondary={condition.type}/>
                                    </ListItem>
                                )
                            }): 
                            <p>No Conditions selected</p>}
                    </List>                
                </AccordionDetails>
            </Accordion>

            <Accordion expanded>
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                    >
                    <Typography sx={{fontSize: 20, fontWeight: 700}}>Medical Questions</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <List>
                        {   
                            formInfoCtx.medicalQuestions.map( (question) => {
                                return (
                                    <ListItem key={question.question}>
                                        <Grid container spacing={3}>
                                            <Grid item xs={7}>
                                                <ListItemText primary={question.question} secondary={question.subquestion}/>
                                            </Grid>
                                            <Grid item xs={5}>
                                                <Typography align='left' className={styles.centered}>
                                                    {
                                                        question.suffer ?  <span>{question.answer}</span> : <span>No</span>
                                                    } 
                                                </Typography>
                                            </Grid>
                                        </Grid>
                                    </ListItem>
                                )
                            })
                        }
                    </List>                
                </AccordionDetails>
            </Accordion>

        </div>
    )
}

export default Summary;