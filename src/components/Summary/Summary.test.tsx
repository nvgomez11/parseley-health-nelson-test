import Enzyme from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import { mount } from 'enzyme';
import FormContextProvider from './../../store/form-context';
import Summary from './Summary';
import Accordion from '@mui/material/Accordion';

Enzyme.configure({ adapter: new Adapter() });
describe('(Component) Summary', () => {
    test('render summary correctly', () => {        
        const wrapper = mount(
            <FormContextProvider>
                <Summary/>
            </FormContextProvider>
        );
        const accordionElements = wrapper.find(Accordion);
        expect(accordionElements.length).toBe(3);
    });
})